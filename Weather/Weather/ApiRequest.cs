﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Weather
{
    internal static class ApiRequest
    {

        public async static void PostRequest(string date, string maxtemperature, string mintemperature, string humidity, string pressure, string description)
        {
            string url = "https://requestbin.net/r/76j8og0s";

            using (var webClient = new WebClient())
            {
                // Создаём коллекцию параметров
                var pars = new NameValueCollection();

                // Добавляем необходимые параметры в виде пар ключ, значение
                pars.Add("date", date);
                pars.Add("maxtemperature", maxtemperature);
                pars.Add("mintemperature", mintemperature);
                pars.Add("humidity", humidity);
                pars.Add("pressure", pressure);
                pars.Add("description", description);

                // Посылаем параметры на сервер
                var response = webClient.UploadValues(url, pars);
                Console.WriteLine("Your message is pussed successed");
            }
        }
    }
}
