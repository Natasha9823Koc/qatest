﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Weather
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("If you wanna pass metcast press 1\n" +
                "If you wanna get metcast press 2");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    User.PassWeather();
                    break;
                case "2":
                    await User.GetWeather();
                    break;
            }
        }

    }
}
