﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Weather
{
    class User
    {
        public static void PassWeather()
        {
            Console.WriteLine("Enter the date you wanna pass");
            string date = Console.ReadLine();
            Console.WriteLine("Enter the max temperature of that day");
            string maxtemperature = Console.ReadLine();
            Console.WriteLine("Enter the min temperature of that day");
            string mintemperature = Console.ReadLine();
            Console.WriteLine("Enter the humidity");
            string humidity = Console.ReadLine();
            Console.WriteLine("Enter the pressure");
            string pressure = Console.ReadLine();
            Console.WriteLine("Enter the description");
            string description = Console.ReadLine();
            ApiRequest.PostRequest(date, maxtemperature, mintemperature, humidity, pressure, description);

        }
        public static async Task GetWeather()
        {
            Console.WriteLine("Enter data you wanna know metcast\n" +
                "format: 00/00/0000");
            string date = Console.ReadLine();
            ApiResponses apiResponses = new ApiResponses();
           await apiResponses.MakeRequests(date);
                 
        }
    }
}
