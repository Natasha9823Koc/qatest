﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Weather
{
     class ApiResponses
    {
        HttpClient client = new HttpClient();
       
        public  async Task MakeRequests(string data)
        {
            string response = await client.GetStringAsync("https://requestbin.net/r/chg504hg?inspect");
            string myEncodedString = HttpUtility.HtmlDecode(response);

            string[] splited_page = myEncodedString.Split("POST PARAMETERS");
            string request="";
            for (int i = 0; i < splited_page.Length; i++)
            {
               if (splited_page[i].Contains(data))
                {
                    request=(splited_page[i]);
                }
            }
            var matches = new Regex(@"<strong>(?<key>\w+):<\/strong> (?<value>[a-zA-Z0-9\/]+)<\/p>").Matches(request);
            Dictionary<string, string> parametrs = new Dictionary<string, string>();
            for (int i = 0; i < matches.Count-1; i++)
            {
                parametrs.Add(matches[i].Groups["key"].ToString(), matches[i].Groups["value"].ToString());
            }
            foreach (var keyvaluepair in parametrs)
            {
                Console.WriteLine(keyvaluepair.Key + " -----> "+keyvaluepair.Value);
                Console.WriteLine();
            }

        }
        


    }
}
