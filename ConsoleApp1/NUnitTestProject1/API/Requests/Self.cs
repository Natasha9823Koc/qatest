﻿using Newtonsoft.Json;
using NUnitTestProject1.API.Models;
using NUnitTestProject1.Support;
using RestSharp;
using System.Collections.Generic;

namespace NUnitTestProject1.API.Requests
{
    internal static class Self
    {
        internal static SelfModel SelfGet(string token)
        {
            ApiRequest.ConfigureApiRequest("https://api.newbookmodels.com/api/v1/client/self/");
            ApiRequest.SetRequestType(Method.GET);

            var headers = new Dictionary<string, string>
            {
                {"authorization", token}
            };
            ApiRequest.SetData(null, headers);

            return JsonConvert.DeserializeObject<SelfModel>(ApiRequest.SendRequest().Content);
        }
        internal static SelfModel GetName(string token)
        {
            ApiRequest.ConfigureApiRequest("https://api.newbookmodels.com/api/v1/client/self/");
            ApiRequest.SetRequestType(Method.GET);

            var headers = new Dictionary<string, string>
            {
                {"authorization", token}
            };
            ApiRequest.SetData(null, headers);
            var temp = ApiRequest.SendRequest().Content;

            return JsonConvert.DeserializeObject<SelfModel>(temp);
        }
    }
}
