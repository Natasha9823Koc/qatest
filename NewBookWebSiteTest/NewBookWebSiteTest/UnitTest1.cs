using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System;

namespace NewBookWebSiteTest
{
    public class Tests
    {
        IWebDriver driver = new ChromeDriver();
        private const string email = "nkocura3@gmail.com";
        private const string password = "Nata_1234";

        private readonly By firstnameInputButton = By.Name("first_name");
        private readonly By lastnameInputButton = By.Name("last_name");
        private readonly By emailInputButton = By.Name("email");
        private readonly By passwordInputButton = By.Name("password");
        private readonly By passwordconfirmInputButton = By.Name("password_confirm");
        private readonly By mobileconfirmInputButton = By.Name("phone_number");
        private readonly By companynameInputButton = By.Name("company_name");
        private readonly By companywebsiteInputButton = By.Name("company_website");
        private readonly By companyaddressInputButton = By.Name("location");
        private readonly By signinButton = By.XPath("//button[text()='Sign Up']");
        private readonly By continueButton = By.XPath("//button[@type='submit']");
        private readonly By industryButton = By.Id("SIGNUP_FIRST_FORM/industry");
        private readonly By industrychoiseButton = By.XPath("//input[@value = 'Advertising']");
        private readonly By finishButton = By.XPath("//button[@type = 'submit']");
        private readonly By userareaButton = By.XPath("//div[@class = 'AvatarClient__avatar--3TC7_']");
        private readonly By updateButton = By.XPath("//a[@href = '/account-settings']");
        private readonly By updateFirstNameInputButton = By.XPath("//common-input[@formcontrolname = 'first_name']");
        private readonly By updateLastNameInputButton = By.XPath("//common-input[@formcontrolname = 'last_name']");
        private readonly By updateCompanyLocationInputButton = By.XPath("//input[@class = 'input__self input__self_type_text-underline ng-pristine ng-valid pac-target-input ng-touched']");
        private readonly By updateIndustryInputButton = By.XPath("//common-input[@formcontrolname = 'industry']");
        private readonly By saveChangesButton = By.XPath("//common-button-deprecated[@class = 'mt-5']");
        [SetUp]
        public void Setup()
        {
            driver.Navigate().GoToUrl("https://newbookmodels.com");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
        }

        [Test]
        public void RegistrationWithValidData()
        {
            var signIn = driver.FindElement(signinButton);
            signIn.Click();
            var firstname = driver.FindElement(firstnameInputButton);
            firstname.SendKeys("Nataly");
            var lastname = driver.FindElement(lastnameInputButton);
            lastname.SendKeys("Kotsyura");
            var email = driver.FindElement(emailInputButton);
            email.SendKeys("ffjhdautuyuie@yahoo-emails.online");
            var password = driver.FindElement(passwordInputButton);
            password.SendKeys("Nata_1234");
            var passwordconfirm = driver.FindElement(passwordconfirmInputButton);
            passwordconfirm.SendKeys("Nata_1234");
            var phone = driver.FindElement(mobileconfirmInputButton);
            phone.SendKeys("543.768.2346");
            var continuebut = driver.FindElement(continueButton);
            continuebut.Click();
            var companyname = driver.FindElement(companynameInputButton);
            companyname.SendKeys("TestCompany");
            var companywebsite = driver.FindElement(companywebsiteInputButton);
            companywebsite.SendKeys("testcompany.com");
            driver.FindElement(companyaddressInputButton).SendKeys("2401");
            Thread.Sleep(4000);
            driver.FindElement(companyaddressInputButton).Click();
            driver.FindElement(companyaddressInputButton).SendKeys(Keys.ArrowDown);
            driver.FindElement(companyaddressInputButton).SendKeys(Keys.Enter);
            driver.FindElement(industryButton).Click(); // Click on dropdown
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", driver.FindElement(industrychoiseButton));
            var finish = driver.FindElement(finishButton);
            finish.Click();
        }
        [TestCase(email, password, "Olha", "Ivanova", "2671", "it")]
        public void AuthorizationAndChangePersonalInfoWithValidData(string email, string password, string new_first_name, string new_last_name, string index, string newindustry)
        {
            driver.FindElement(userareaButton).Click();
            driver.FindElement(updateButton).Click();
            var firstname = driver.FindElement(updateFirstNameInputButton);
            firstname.Clear();
            firstname.SendKeys(new_first_name);
            var lastname = driver.FindElement(updateLastNameInputButton);
            lastname.Clear();
            lastname.SendKeys(new_last_name);
            var companylocation = driver.FindElement(updateCompanyLocationInputButton);
            companylocation.Clear();
            companylocation.SendKeys(index);
            Thread.Sleep(2000);
            driver.FindElement(companyaddressInputButton).Click();
            Thread.Sleep(2000);
            driver.FindElement(companyaddressInputButton).SendKeys(Keys.ArrowDown);
            Thread.Sleep(2000);
            var industry = driver.FindElement(updateIndustryInputButton);
            industry.Clear();
            industry.SendKeys(newindustry);
            driver.FindElement(saveChangesButton).Click();
        }
        [TearDown]
        public void TearDown()
        {
            Thread.Sleep(4000);
            driver.Quit();
        }
    }
}